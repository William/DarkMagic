### NCurses Game ###

A game engine and game made using NCurses

Ascii characters: https://www.asciitable.com/

NCurses:
- https://www.youtube.com/watch?v=lV-OPQhPvSM&list=PL2U2TQ__OrQ8jTf0_noNKtHMuYlyxQl4v
- https://tldp.org/HOWTO/NCURSES-Programming-HOWTO/

Todo: 
- add tinyxml2 and toml support
- smooth windowing
- create menu inheritance
- add Jenkins
