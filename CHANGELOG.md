
### Change Log ###

Just a simple journal to keep track of changes or easily, please add new 
changes to the top of the file, formating is below.

Author: Actual name is prefered but a consistant alias is ok.
Branch: What branch the commit is being added to.
Date & Time: dd/mm/yyyy hh:mm am/pm
Description: Elaborate on the commit message and possbile issue that may arise because of it.

-------------------------------------------------------------------------------

Author: William Behrens
Branch: master
Date & Time: 11/2/2021 7:43 pm
Description: re-organized the project to make it more maintainable.
