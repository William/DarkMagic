#include "Menu.hpp"
#include "Window.hpp"

#include <iostream>
#include <ncurses.h>

Menu::Menu(int scrHeight, int scrWidth, int scrStartX, int scrStartY) : Window(scrHeight, scrWidth, scrStartX, scrStartY) {
	this->drawBorder('|', '~', '+');
}

Menu::~Menu() {
	this->destroy();
}

void Menu::destroyMenu() {
	this->~Menu();
}

int Menu::drawMenu(std::string vals[]) {
	this->refreshWin();
	this->takeKeypad();

	int selected;
	int highlight = 0;

	while( 1 ) {
		for( int i = 0; i < vals->size(); i++ ) {
			if( i == highlight ) { this->setAtter(A_REVERSE); }
			this->print(i + 1, 1, vals[i].c_str());
			this->releaseAtter(A_REVERSE);
		}

		selected = this->getChar();
		switch( selected ) {
			default:
				break;

			case KEY_UP:
				highlight--;
				if( highlight == -1 ) { highlight = 0; }
				break;

			case KEY_DOWN:
				highlight++;
				if( highlight == (vals->size()) ) { highlight = vals->size() - 1; }
				break;
		}

		if( selected == 10 ) { break; }
	}
	return highlight;
}
