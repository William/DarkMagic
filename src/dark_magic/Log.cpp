#include "Log.hpp"
//#include <cstdlib>
#include <iostream>

namespace log {

	int logLevel = 1;

	void error(const char* msg) {
		std::cerr << "ERROR: " << msg << std::endl;
		std::exit(-1);
	}
	void warning(const char* msg) {
		if( logLevel <= 2 ) { std::clog << "WARNING: " << msg << std::endl; }
	}
	void message(const char* msg) {
		if( logLevel <= 1 ) { std::clog << "lOG: " << msg << std::endl; }
	}
	void changeLogLevel(int newLogLevel) {}
} // namespace log
