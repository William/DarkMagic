#include "Window.hpp"
#include "Log.hpp"

#include <iostream>
#include <ncurses.h>


// Constructor:
Window::Window(int winHeight, int winWidth, int startX, int startY) {
	this->_win = newwin(winHeight, winWidth, startX, startY);
	if( !has_colors() ) { log::error("Terminal doesn't support color"); }
}

// Destructor: clears Window, then ends windows
Window::~Window() {
	this->releaseColors();
	endwin();
}

void Window::destroy() {
	this->~Window();
}

//#############################################################################

void Window::refreshWin() {
	wrefresh(this->_win);
}

void Window::wipe() {
	wclear(this->_win);
}

//#############################################################################

void Window::drawBorder(char leftRight, char topBottem, char corners) {
	// box(this->win, leftRight, topBottem);
	wborder(this->_win, leftRight, leftRight, topBottem, topBottem, corners, corners, corners, corners);
	refreshWin();
}

//#############################################################################

void Window::setColors(short fgColor, short bgColor) {
	init_pair(1, fgColor, bgColor);
	wattron(this->_win, COLOR_PAIR(1));
}

// This function is kinda stupid and baically acts as a beta draw, why?
// I think that it needs to be surrounding the item it affects and thus handles all of this stuff
void Window::releaseColors() {
	wattroff(this->_win, COLOR_PAIR(1));
}

//#############################################################################

void Window::setAtter(attr_t attr) {
	wattr_on(this->_win, attr, nullptr); // TODO: check back on this later to see if nullptr is right
}
void Window::releaseAtter(attr_t attr) {
	wattroff(this->_win, attr);
}

//#############################################################################

int Window::getCols() {
	return getmaxx(this->_win);
}

int Window::getRows() {
	return getmaxy(this->_win);
}

//#############################################################################

int Window::getChar() {
	return wgetch(this->_win);
}

//#############################################################################

void Window::print(int y, int x, const char *text) {
	mvwprintw(this->_win, y, x, text);
}

//#############################################################################

void Window::takeKeypad() {
	if( (keypad(this->_win, true)) ) {
		// log error
		std::cout << "failed to init keypad\n";
	}
}
