#include "Screen.hpp"
#include "Log.hpp"

#include "Log.hpp"
#include "ncurses.h"

// Constructor:
Screen::Screen() {
	initscr();
	// int scrHeight, scrWidth;
	// getmaxyx( stdscr, scrHeight, scrWidth );
	// this->_win = newwin( scrHeight, scrWidth, 0, 0 );
	if( has_colors() ) {
		start_color();
	} else {
		log::error("Terminal doesn't support color");
	}
	cbreak();
	noecho();
}

// Destructor: clears screen, refreshes screen, then ends windows
Screen::~Screen() {
	clear();
	this->refreshScr();
	endwin();
}

//########################################################################

void Screen::refreshScr() {
	refresh();
	wrefresh(stdscr);
}

void Screen::wipe() {
	wclear(stdscr);
}

//########################################################################

void Screen::drawBorder(char leftRight, char topBottem, char corners) {
	// box(this->win, leftRight, topBottem);
	wborder(stdscr, leftRight, leftRight, topBottem, topBottem, corners, corners, corners, corners);
	refreshScr();
}

//########################################################################

void Screen::setColors(short fgColor, short bgColor) {
	init_pair(1, fgColor, bgColor);
	wattron(stdscr, COLOR_PAIR(1));
}

// This function is kinda stupid and baically acts as a beta draw, why?
// I think that it needs to be surrounding the item it affects and thus handles all of this stuff
void Screen::releaseColors() {
	wattroff(stdscr, COLOR_PAIR(1));
}

//########################################################################

void Screen::setAtter() {}
void Screen::releaseAtter() {}

//########################################################################

int Screen::getCols() {
	return getmaxx(stdscr);
}

int Screen::getRows() {
	return getmaxy(stdscr);
}
