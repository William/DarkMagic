#pragma once

namespace log {

	void error(const char* msg);

	void warning(const char* msg);

	void message(const char* msg);

	void changeLogLevel(int newLogLevel);

} // namespace log
