#pragma once

#include "Window.hpp"
#include "ncurses.h"

#include <ncurses.h>
#include <string>

class Menu : protected Window {
	public:
	Menu(int scrHeight, int scrWidth, int scrStartX, int scrStartY);
	~Menu();

	int drawMenu(std::string vals[]);
	void clearMenu();
	void destroyMenu();
};