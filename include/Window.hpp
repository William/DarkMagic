#pragma once

#include <ncurses.h>

class Window {
	public:
	Window(int winHeight, int winWidth, int startX, int startY);
	// Window();
	~Window();

	void destroy();
	void refreshWin();
	void releaseColors();
	void drawBorder(char leftRight, char topBottem, char corners);
	void setColors(short fgColor, short bgColor);
	void wipe();
	void print(int y, int x, const char *text);
	void setAtter(attr_t attr); // TODO: check back on this
	void releaseAtter(attr_t attr);
	void takeKeypad();

	int getCols();
	int getRows();

	int getChar();

	protected:
	WINDOW *_win;
};
