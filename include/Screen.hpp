#pragma once

#include "ncurses.h"

class Screen {
	public:
	// Screen( int scrHeight, int scrWidth, int scrStartX, int scrStartY );
	Screen();
	~Screen();

	void refreshScr();
	void releaseColors();
	void drawBorder(char leftRight, char topBottem, char corners);
	void setColors(short fgColor, short bgColor);
	void setAtter();
	void releaseAtter();
	void wipe();

	int getCols();
	int getRows();
};
