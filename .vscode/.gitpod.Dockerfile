# Gitpod docker image
FROM gitpod/workspace-full

# User
USER gitpod

# Create a clean folder to build in at /home/gitpod/
#RUN mkdir -p /home/gitpod/BuildSpace
#WORKDIR /home/gitpod/BuildSpace

RUN sudo apt-get update && sudo apt-get install -y clangd lldb ruby clang libncurses-dev libtinyxml2-dev
#RUN sudo gem install solargraph
